# reactjs_bloc

BLoC pattern for Reactjs.
If you need BLoC pattern for general javascript project, please use js_bloc: [js_bloc](https://www.npmjs.com/package/js_bloc)

# Install:
* yarn add reactjs_bloc
* npm i reactjs_bloc

# Usage:
1. BlocState
```
//MyState.ts
import {BlocState} from "reactjs_bloc"
//Base State
export class MyState extends BlocState {
    //Must have toString method!!!
    //Using for compare two different classes
    //Reason: After minify Javascript source code, two classes may be have same name!!!
    toString = () => "MyState"
}
export class InitialState extends MyState {
    //Must have toString method!!!
    //Using for compare two different classes
    //Reason: After minify Javascript source code, two classes may be have same name!!!
    toString = () => "InitialMyState"
}
export class RunningState extends MyState {
    //Must have toString method!!!
    //Using for compare two different classes
    //Reason: After minify Javascript source code, two classes may be have same name!!!
    toString = () => "RunningState"
    speed: number
    get props() {
        return [this.speed]
    }
}
```
2. BlocEvent: similar to BlocState
```
//MyEvent.ts
```
3. Bloc
```
//MyBloc.ts
import { Stream, Bloc } from "reactjs_bloc";
import {MyState, AuthState2, LoadingMyState} from "./MyState"
import {MyEvent, AppStartedMyEvent, MyEvent2} from "./MyEvent"
class MyBloc extends Bloc<MyEvent, MyState> {
    /**
     * Constructor
     */
    constructor() {
        super(new InitialState())
    }
    /**
     * Map event to state
     */
    async *mapEventToState(event: MyEvent): Stream<MyState> {
        //Event --> yield new state
        if (event instanceof AppStartedMyEvent) {
            //Do somthing here...
            ...
            //New sate
            yield new LoadingMyState()
        }
        //Event 2
        if (event instanceof MyEvent2) {
            //Do somthing here...
            ...
            //New sate
            yield new AuthState2()
        }
    }
}
//Export
export default MyBloc
```
4. Usage:
```
    /**
     * My bloc
     */
    const myBloc = new MyBloc();
    //Listen change
    myBloc.listen((state: MyState) => {
        console.log("My state change: ", state)
    })
    //Add new event
    myBloc.add(new AppStartedMyEvent)
    //Close
    myBloc.close()
```

5. BlocProvider, BlocBuilder, BlocListener

```
class Page extends CoreComponent {

    /**
     * Render
     */
    render() {
        const body = this.body()
        return (
            <BuildContextProvider>
                <BlocProvider create={() => authBloc} autoClose={false}>
                    {body}
                </BlocProvider>
            </BuildContextProvider>
        )
    }
}
<BlocBuilder<AuthBloc, AuthState> builder={(state: AuthState) => {
    buildWhen = {(prevState: AuthState, state: AuthState) => {
        return true;
    }}
    if (state instanceof AuthenticatedAuthState) {
        return (
        <div>
            <p>Hello {state.user.ho_ten} <a href="#" onClick={() => authBloc.add(new LogoutAuthEvent())}>Logout</a></p>
            <p><img src={state.user.avatar} /></p>
            
        </div>
        )
    }
    return (
        <A href="/login" className="card">
        <h1>Login</h1>
        </A>
    )
    }} />
<BlocListener<AuthBloc, AuthState>
    buildWhen = {(prevState: AuthState, state: AuthState) => {
        return true;
    }}
    listener={this.authStateListener}>
/>
<BlocListener<AuthBloc, AuthState>
    buildWhen = {(prevState: AuthState, state: AuthState) => {
        return true;
    }}
    listener={this.authStateListener}>
    <div className="container">
        Children
    </div>
</BlocListener>
```
