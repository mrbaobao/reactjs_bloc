import { BehaviorSubject, Subscription } from "rxjs"
import { BlocEvent } from "./Event"
import { BlocState } from "./State"

/**
 * class: Change
 */
export class Change<StateType extends BlocState> {
  currentState: StateType
  nextState: StateType
  constructor(currentState: StateType,
    nextState: StateType) {
    this.currentState = currentState
    this.nextState = nextState
  }
}
/**
 * class: Transition
 */
export class Transition<EventType extends BlocEvent, StateType extends BlocState> {
  currentState: StateType
  nextState: StateType
  event: EventType
  constructor(currentState: StateType, event: EventType,
    nextState: StateType) {
    this.currentState = currentState
    this.nextState = nextState
    this.event = event
  }
}
/**
 * main class: Bloc
 */
export abstract class Bloc<
  EventType extends BlocEvent,
  StateType extends BlocState
  > {
  //keep track of the events coming in
  private _events: BehaviorSubject<EventType | null> = new BehaviorSubject<EventType | null>(null);
  //keep track of the states going out
  private _states: BehaviorSubject<StateType>;
  public state: StateType;

  //the observable other stuff will bind to to get our current state
  // public state: Observable<StateType>;
  public InitialState: StateType;

  constructor(initialState: StateType) {
    //we need an initial state for our behaviour subjects
    this.state = this.InitialState = initialState;

    //init our subject
    this._states = new BehaviorSubject<StateType>(initialState);
    //init our external observable that stuff listens to
    // this.state = this._states;

    //every time we get a new event run the child method
    this._events.subscribe((event: EventType | null) => {
      if (!event) {
        return
      }
      this.dispatchEvent(event);
    });
  }

  //async read all the yields coming from the child method
  private async dispatchEvent(event: EventType) {
    var stateIterator = this.mapEventToState(event);
    //fancy shizzle to keep reading the asyncgenerator
    for await (let state of stateIterator) {
      if (!this.state.equals(state)) {
        this.onTransition(new Transition(this.state, event, state))
      }
    }
  }
  //receive new events
  public add(event: EventType) {
    //add the event
    this._events.next(event);
  }

  //call inherited classes method
  abstract mapEventToState(event: EventType): AsyncGenerator<StateType>;
  //stop observations
  public close() {
    this._events.complete();
    this._states.complete();
  }

  //Listen to state changes
  public listen(listener: (state: StateType) => void): Subscription {
    return this._states.subscribe(listener);
  }
  //Call when state changed, after onTransition
  //@needs-call-super
  protected onChange(change: Change<StateType>): void {
    this.state = change.nextState
    this._states.next(change.nextState)
  }
  //Call when state changed
  //@needs-call-super
  protected onTransition(transition: Transition<EventType, StateType>): void {
    this.onChange(new Change(transition.currentState, transition.nextState))
  }
}
