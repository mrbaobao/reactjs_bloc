import * as React from "react"
import { Bloc, BlocEvent, BlocState } from ".."
import { BuildContext, BuildContextValue } from "./Context/BuildContext"

/**
 * BlocProvider
 */
interface BlocProviderProps {
    create: () => Bloc<BlocEvent, BlocState>
    autoClose?: boolean
    children: JSX.Element | JSX.Element[]
}

//Class: BlocProvider
class BlocProvider extends React.Component<BlocProviderProps> {
    //Context type
    static contextType = BuildContext
    bloc?: Bloc<BlocEvent, BlocState>
    /**
     * Constructor
     * - Create bloc
     * @param props 
     */
    constructor(props: BlocProviderProps, context: BuildContextValue) {
        super(props)
        //Init bloc
        this.initBloc(props, context)
    }
    /**
     * Stop listener
     */
    componentWillUnmount() {
        if (this.props.autoClose !== false) {
            this.bloc?.close()
        }
    }
    /**
     * Init bloc
     * @param props 
     */
    initBloc(props: BlocProviderProps, context: BuildContextValue) {
        //Build context
        const buildContext = context.buildContext
        if (!buildContext || !context.setBuildContext) {
            return
        }
        //Create bloc
        this.bloc = this.create(props)
        //Add bloc to context
        buildContext.addBloc(this, this.bloc)
        //Update
        context.setBuildContext(buildContext)
    }
    /**
     * Query a bloc
     */
    static of<B extends Bloc<BlocEvent, BlocState>>(context: BuildContextValue): B | null {
        return context?.buildContext?.bloc<B>() ?? null
    }
    /**
     * Builder function from context and current state
     * @mustOverride
     */
    create(props: BlocProviderProps): Bloc<BlocEvent, BlocState> {
        return props.create()
    }
    /**
     * Render
     */
    render(): React.ReactNode {
        return this.props.children
    }
}
//Export
export default BlocProvider
