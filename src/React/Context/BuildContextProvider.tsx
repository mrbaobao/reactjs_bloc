import * as React from "react"
import { BuildContext } from "./BuildContext"
import BuildContextData from "./BuildContextData"

/**
 * BuildContext provider
 */
class BuildContextProvider extends React.Component<any, any> {
    //Constructor
    constructor(props: any) {
        super(props)
        //State
        this.state = {
            buildContext: new BuildContextData()
        }
    }
    //Set build context
    setBuildContext = (buildContext: BuildContextData) => {
        this.setState({
            buildContext
        })
    }
    //Render
    render() {
        const { buildContext } = this.state
        return (
            <BuildContext.Provider value={{ buildContext, setBuildContext: this.setBuildContext }}>
                {this.props.children}
            </BuildContext.Provider>
        )
    }
}
//Export
export default BuildContextProvider