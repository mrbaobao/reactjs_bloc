import { Bloc, BlocEvent, BlocState } from "../.."
/**
 * BuildContextData
 */
class BuildContextData {
    blocs: Map<Object, Bloc<BlocEvent, BlocState>> = new Map()
    constructor(provider?: any, bloc?: Bloc<BlocEvent, BlocState>) {
        this.addBloc(provider, bloc)
    }
    /**
     * Add bloc
     */
    addBloc(provider?: any, bloc?: Bloc<BlocEvent, BlocState>) {
        if (provider && bloc) {
            this.blocs.set(provider, bloc)
        }
    }
    /**
     * Query bloc with type B
     */
    bloc<B extends Bloc<BlocEvent, BlocState>>(): B {
        for(let bloc of this.blocs.values()) {
            try {
                return bloc as B
            } catch (_) {}
        }
        //Error not found bloc
        throw `
            BlocContextData.bloc:
            Could not found any Bloc with the type.
            The context used was: ${this}.
        `
    }
}
//Export
export default BuildContextData