import * as React from "react"
import BuildContextData from "./BuildContextData"

//Context
class BuildContextValue {
    buildContext?: BuildContextData
    setBuildContext?: (buildContext: BuildContextData) => void
}
const BuildContext = React.createContext<BuildContextValue>(new BuildContextValue())
BuildContext.displayName = "BuildContext"

//Exports
export { BuildContext, BuildContextValue }