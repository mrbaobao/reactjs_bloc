import * as React from "react"
import { Subscription } from "rxjs"
import { Bloc, BlocEvent, BlocState } from ".."
import BlocProvider from "./BlocProvider"
import { BuildContext, BuildContextValue } from "./Context/BuildContext"

/**
 * BlocListener
 */
interface BlocListenerProps<S extends BlocState> {
    bloc?: Bloc<BlocEvent, BlocState>
    listenWhen?: (prevState: S, state: S) => boolean
    listener?: (state: S) => void
    children: JSX.Element | JSX.Element[]
}
interface BlocListenerState<S extends BlocState> {
    blocState: S
}
class BlocListener<B extends Bloc<BlocEvent, BlocState>, S extends BlocState> extends React.Component<BlocListenerProps<S>, BlocListenerState<S>> {
    blocState?: S
    //Context type
    static contextType = BuildContext
    declare context: React.ContextType<typeof BuildContext>
    subscriber?: Subscription
    /**
     * Constructor
     * 1. init blocState
     * @param props
     */
    constructor(props: BlocListenerProps<S>, context: BuildContextValue) {
        super(props)
    }
    /**
     * Listen in client side
     */
    componentDidMount() {
        //Listen bloc
        this.listenBloc(this.props, this.context)
    }
    /**
     * Close listener
     */
    componentWillUnmount() {
        this.subscriber?.unsubscribe()
    }
    getBloc = (props: BlocListenerProps<S> = this.props, context: BuildContextValue = this.context) => {
        return props.bloc ?? BlocProvider.of<B>(context)
    }
    /**
     * listen bloc state change
     */
    listenBloc(props: BlocListenerProps<S>, context: BuildContextValue) {
        const bloc = this.getBloc(props, context)
        if (!bloc) {
            throw "BlocListener not found Bloc."
        }
        this.subscriber = bloc.listen((state: BlocState) => {
            const newState = state as S
            let needsListen = true
            if (this.blocState) {
                needsListen = this.listenWhen(this.blocState, newState)
                if (props.listenWhen !== undefined) {
                    needsListen = props.listenWhen(this.blocState, newState)
                }
            }

            //Check needs listen or not
            if (needsListen) {
                if (props.listener !== undefined) {
                    props.listener(newState)
                } else {
                    this.listener(props, newState)
                }
                //Update new state
                this.blocState = newState
            }
        })
    }
    /**
     * Decide whenever listen state change
     * @param prevState 
     * @param state 
     */
    listenWhen = (prevState: S, state: S): boolean => {
        return true
    }
    /**
     * Listen state change
     */
    listener(props: BlocListenerProps<S>, state: S): void {
        return
    }
    /**
     * Render
     */
    render(): React.ReactNode {
        return this.props.children ?? null
    }
}
//Export
export default BlocListener
