import * as React from "react"
import { Subscription } from "rxjs"
import { Bloc, BlocEvent, BlocState } from ".."
import BlocProvider from "./BlocProvider"
import { BuildContext, BuildContextValue } from "./Context/BuildContext"

/**
 * BlocBuilder
 */
interface BlocBuilderProps<S> {
    bloc?: Bloc<BlocEvent, BlocState>
    builder?: (state?: S) => React.ReactNode
    buildWhen?: (prevState: S, state: S) => boolean
}
interface BlocBuilderState<S extends BlocState> {
    blocState?: S
}
class BlocBuilder<B extends Bloc<BlocEvent, BlocState>, S extends BlocState> extends React.Component<BlocBuilderProps<S>, BlocBuilderState<S>> {
    static contextType = BuildContext
    declare context: React.ContextType<typeof BuildContext>
    subscriber?: Subscription
    /**
     * Constructor
     * 1. init blocState
     * @param props
     */
    constructor(props: BlocBuilderProps<S>, context: BuildContextValue) {
        super(props)
        let blocState: S | undefined
        if (props.bloc?.InitialState) {
            blocState = props.bloc?.InitialState as S
        }
        this.state = {
            blocState,
        }
    }
    componentDidMount() {
        //Listen bloc
        this.listenBloc(this.props, this.context)
    }
    /**
     * Close listener
     */
    componentWillUnmount() {
        this.subscriber?.unsubscribe()
    }
    getBloc = (props: BlocBuilderProps<S> = this.props, context: BuildContextValue = this.context) => {
        return props.bloc ?? BlocProvider.of<B>(context)
    }
    /**
     * listen bloc state change
     */
    listenBloc(props: BlocBuilderProps<S>, context: BuildContextValue) {
        const bloc = this.getBloc(props, context)
        if (!bloc) {
            throw "BlocBuilder not found Bloc."
        }
        this.subscriber = bloc.listen((state: BlocState) => {
            const newState = state as S
            let needsBuild = true
            if (this.state.blocState) {
                needsBuild = this.builderWhen(this.state.blocState, newState)
                if (props.buildWhen !== undefined) {
                    needsBuild = props.buildWhen(this.state.blocState, newState)
                }
            }

            //Check needs build or not
            if (needsBuild) {
                this.setState({
                    blocState: newState
                })
            }
        })
    }
    /**
     * builder when
     */
    builderWhen = (prevState: S, state: S): boolean => {
        return true
    }
    /**
     * Builder function from context and current state
     */
    builder(state?: S): React.ReactNode {
        //Null
        return null
    }
    /**
     * Render
     */
    render() {
        if (this.props.builder !== undefined) {
            return this.props.builder(this.state.blocState)
        }
        return this.builder(this.state.blocState)
    }
}
//Export
export default BlocBuilder
